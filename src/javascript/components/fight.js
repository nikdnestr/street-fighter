import { controls } from '../../constants/controls';

const {
  PlayerOneAttack,
  PlayerOneBlock,
  PlayerTwoAttack,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

export async function fight(playerOne, playerTwo) {
  setInitialState(playerOne, playerTwo);

  return new Promise((resolve) => {
    document.addEventListener('keydown', keyDownHandler.bind(null, playerOne, playerTwo, resolve));
    document.addEventListener('keyup', keyUpHandler);
  });
}

//state

export const state = {
  playerOne: {},
  playerTwo: {},
  playerOneHealth: null,
  playerTwoHealth: null,
  playerOneBlocking: false,
  playerTwoBlocking: false,
  playerOneCritCooldown: null,
  playerTwoCritCooldown: null,
  playerOneInput: [],
  playerTwoInput: [],
  hud: null,
};

function setInitialState(playerOne, playerTwo) {
  state.playerOne = playerOne;
  state.playerTwo = playerTwo;
  state.playerOneHealth = playerOne.health;
  state.playerTwoHealth = playerTwo.health;

  const healthIndicator = document.getElementById(`left-fighter-indicator`);
  state.hud = healthIndicator.clientWidth;
}

//input handlers

function keyDownHandler(playerOne, playerTwo, resolve, event) {
  const key = event.code;
  let args;

  switch (key) {
    case PlayerOneAttack:
      if (state.playerOneBlocking) return;
      args = state.playerTwoBlocking ? [playerOne, playerTwo] : [playerOne];
      decreaseHealth(true, getDamage(...args));
      updateHUD(true);
      break;
    case PlayerTwoAttack:
      if (state.playerTwoBlocking) return;
      args = state.playerOneBlocking ? [playerTwo, playerOne] : [playerTwo];
      decreaseHealth(false, getDamage(...args));
      updateHUD(false);
      break;
    case PlayerOneBlock:
      state.playerOneBlocking = true;
      break;
    case PlayerTwoBlock:
      state.playerTwoBlocking = true;
      break;
  }

  if (PlayerOneCriticalHitCombination.includes(key) && !state.playerOneInput.includes(key)) {
    state.playerOneInput.push(key);
  } else if (PlayerTwoCriticalHitCombination.includes(key) && !state.playerTwoInput.includes(key)) {
    state.playerTwoInput.push(key);
  }

  if (state.playerOneInput.length === 3 && state.playerOneCritCooldown + 10000 < Date.now()) {
    decreaseHealth(true, getCriticalHitPower(playerOne));
    updateHUD(true);
    state.playerOneCritCooldown = Date.now();
  } else if (state.playerTwoInput.length === 3 && state.playerTwoCritCooldown + 10000 < Date.now()) {
    decreaseHealth(false, getCriticalHitPower(playerTwo));
    updateHUD(false);
    state.playerTwoCritCooldown = Date.now();
  }

  if (state.playerOneHealth < 0) {
    resolve(playerTwo);
  } else if (state.playerTwoHealth < 0) {
    resolve(playerOne);
  }
}

function keyUpHandler(event) {
  const key = event.code;

  switch (key) {
    case PlayerOneBlock:
      state.playerOneBlocking = false;
      break;
    case PlayerTwoBlock:
      state.playerTwoBlocking = false;
      break;
  }

  if (state.playerOneInput.includes(key)) {
    const pos = state.playerOneInput.indexOf(key);
    state.playerOneInput.splice(pos, 1);
  } else if (state.playerTwoInput.includes(key)) {
    const pos = state.playerTwoInput.indexOf(key);
    state.playerTwoInput.splice(pos, 1);
  }
}

//calculations

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const hitPower = fighter.attack * (Math.random() + 1);
  return hitPower;
}

export function getBlockPower(fighter) {
  if (!fighter) return 0;
  const blockPower = fighter.defense * (Math.random() + 1);
  return blockPower;
}

export function getCriticalHitPower(fighter) {
  const criticalHitPower = fighter.attack * 2;
  return criticalHitPower;
}

export function decreaseHealth(isPlayerOneStrike, damage) {
  const currentHealth = isPlayerOneStrike ? 'playerTwoHealth' : 'playerOneHealth';
  state[currentHealth] -= damage;
}

export function updateHUD(isPlayerOneStrike) {
  const defender = isPlayerOneStrike ? state.playerTwo : state.playerOne;
  const currentHealth = isPlayerOneStrike ? state.playerTwoHealth : state.playerOneHealth;
  const position = isPlayerOneStrike ? 'right' : 'left';

  const healthIndicator = document.getElementById(`${position}-fighter-indicator`);

  const newWidth = (state.hud * currentHealth) / defender.health;
  healthIndicator.style.width = newWidth > 0 ? newWidth + 'px' : 0;
}



