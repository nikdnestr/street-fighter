import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const details = createElement({ tagName: 'div', className: 'fighter-preview___info' });
    details.insertAdjacentHTML(
      'beforeend',
      `
      <h2>${fighter.name}</h2>
      <p">${fighter.health} HP</p>
      <p">${fighter.attack} DMG</p>
      <p">${fighter.defense} DEF</p>
    `
    );

    fighterElement.append(fighterImage, details);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
